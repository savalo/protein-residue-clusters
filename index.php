<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01">
<HTML>
<HEAD>
<TITLE>Protein residue clusters</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-2">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Description"
	CONTENT="Protein residues page - you can check cluster or clusters of your favourite protein :)">
<META NAME="Keywords"
	CONTENT="protein,residue,cluster,clusters,residues,proteins">
<META NAME="Author" CONTENT="Matyas Dvorak (savalo) www.mad-dev.com">
<LINK HREF="styles.css" TYPE="text/css" REL="StyleSheet">
</HEAD>

<BODY>

<?php
function ukazjednicky($sekvence) {
	switch ($sekvence) {
		case "F" :
			$sekvence = '<FONT class="jednicky">F</FONT>';
			break;
		case "H" :
			$sekvence = '<FONT class="jednicky">H</FONT>';
			break;
		case "W" :
			$sekvence = '<FONT class="jednicky">W</FONT>';
			break;
		case "Y" :
			$sekvence = '<FONT class="jednicky">Y</FONT>';
			break;
	}
	return $sekvence;
}
function ukazdvojky($sekvence) {
	switch ($sekvence) {
		case "I" :
			$sekvence = '<FONT class="dvojky">I</FONT>';
			break;
		case "L" :
			$sekvence = '<FONT class="dvojky">L</FONT>';
			break;
	}
	return $sekvence;
}
function ukaztrojky($sekvence) {
	switch ($sekvence) {
		case "A" :
			$sekvence = '<FONT class="trojky">A</FONT>';
			break;
		case "G" :
			$sekvence = '<FONT class="trojky">G</FONT>';
			break;
		case "V" :
			$sekvence = '<FONT class="trojky">V</FONT>';
			break;
	}
	return $sekvence;
}
function zvyraznit($sekvence) {
	switch ($sekvence) {
		case "F" :
			$sekvence = '<FONT class="jednicky">F</FONT>';
			break;
		case "H" :
			$sekvence = '<FONT class="jednicky">H</FONT>';
			break;
		case "W" :
			$sekvence = '<FONT class="jednicky">W</FONT>';
			break;
		case "Y" :
			$sekvence = '<FONT class="jednicky">Y</FONT>';
			break;
		case "I" :
			$sekvence = '<FONT class="dvojky">I</FONT>';
			break;
		case "L" :
			$sekvence = '<FONT class="dvojky">L</FONT>';
			break;
		case "A" :
			$sekvence = '<FONT class="trojky">A</FONT>';
			break;
		case "G" :
			$sekvence = '<FONT class="trojky">G</FONT>';
			break;
		case "V" :
			$sekvence = '<FONT class="trojky">V</FONT>';
			break;
	}
	return $sekvence;
}
function jednickypozice($sekvence, $index) {
	switch ($sekvence) {
		case "F" :
			echo '<BR><FONT class="jednicky">F</FONT> pozice ' . $index;
			break;
		case "H" :
			echo '<BR><FONT class="jednicky">H</FONT> pozice ' . $index;
			break;
		case "W" :
			echo '<BR><FONT class="jednicky">W</FONT> pozice ' . $index;
			break;
		case "Y" :
			echo '<BR><FONT class="jednicky">Y</FONT> pozice ' . $index;
			break;
	}
}
function dvojkypozice($sekvence, $index) {
	switch ($sekvence) {
		case "I" :
			echo '<BR><FONT class="dvojky">I</FONT> pozice ' . $index;
			break;
		case "L" :
			echo '<BR><FONT class="dvojky">L</FONT> pozice ' . $index;
			break;
	}
	return $sekvence;
}
function trojkypozice($sekvence, $index) {
	switch ($sekvence) {
		case "A" :
			echo '<BR><FONT class="trojky">A</FONT> pozice ' . $index;
			break;
		case "G" :
			echo '<BR><FONT class="trojky">G</FONT> pozice ' . $index;
			break;
		case "V" :
			echo '<BR><FONT class="trojky">V</FONT> pozice ' . $index;
			break;
	}
	return $sekvence;
}
function jednickypozicepole($protein) {
	$jp_pole = array ();
	
	// projde celou sekvenci proteinu
	for($j = 0; $j < strlen ( $protein ); $j ++) {
		switch ($protein [$j]) {
			case "F" :
				$jp_pole [] = $j + 1;
				break;
			case "H" :
				$jp_pole [] = $j + 1;
				break;
			case "W" :
				$jp_pole [] = $j + 1;
				break;
			case "Y" :
				$jp_pole [] = $j + 1;
				break;
		}
	}
	
	return $jp_pole;
}
function vzdvpoli($pole) {
	// projde cele pole
	for($i = 0; $i < count ( $pole ); $i ++) {
		if ($pole [$i + 1] - $pole [$i] <= 2 && $i != count ( $pole ) - 1) {
			echo '<BR><B>vzdalenost</B> ' . $pole [$i + 1] . ' <B>od</B> ' . $pole [$i] . ' <B> je mensi nebo = 2</B>';
		}
	}
}
function vzdvpolipole($pole) {
	$vvp_pole = array ();
	
	// projde cele pole
	for($v = 0; $v < count ( $pole ); $v ++) {
		if ($pole [$v + 1] - $pole [$v] <= 2 && $v != count ( $pole ) - 1) {
			// ulozi sousedni do pole
			$vvp_pole [] = $pole [$v + 1];
			$vvp_pole [] = $pole [$v];
		}
	}
	
	// odstrani duplicity
	$unikatni = array_unique ( $vvp_pole );
	// setridi pole a preindexuje
	sort ( $unikatni );
	
	return $unikatni;
}
function jednickypucky($sekvence, $polepucku) {
	for($i = 0; $i < strlen ( $sekvence ); $i ++) {
		$styl = false;
		
		for($j = 0; $j < count ( $polepucku ); $j ++) {
			if ($i + 1 == $polepucku [$j]) {
				// echo '<BR>'.$i.' pp:'.$polepucku[$j];
				echo '<U>';
				$styl = true;
			}
		}
		
		switch ($sekvence [$i]) {
			case "F" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">F</FONT>';
				else
					echo '<FONT class="jednicky">F</FONT>';
				break;
			case "H" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">H</FONT>';
				else
					echo '<FONT class="jednicky">H</FONT>';
				break;
			case "W" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">W</FONT>';
				else
					echo '<FONT class="jednicky">W</FONT>';
				break;
			case "Y" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">Y</FONT>';
				else
					echo '<FONT class="jednicky">Y</FONT>';
				break;
			
			case "I" :
				echo '<FONT class="dvojky">I</FONT>';
				break;
			case "L" :
				echo '<FONT class="dvojky">L</FONT>';
				break;
			
			case "A" :
				echo '<FONT class="trojky">A</FONT>';
				break;
			case "G" :
				echo '<FONT class="trojky">G</FONT>';
				break;
			case "V" :
				echo '<FONT class="trojky">V</FONT>';
				break;
			default :
				echo $sekvence [$i];
		}
		
		for($j = 0; $j < count ( $polepucku ); $j ++) {
			if ($i + 1 == $polepucku [$j]) {
				echo '</U>';
			}
		}
	}
}
// #############################################################################
function pozicepole($protein) {
	$jp_pole = array ();
	
	// projde celou sekvenci proteinu
	for($j = 0; $j < strlen ( $protein ); $j ++) {
		switch ($protein [$j]) {
			case "F" :
				$jp_pole [] = $j + 1;
				break;
			case "H" :
				$jp_pole [] = $j + 1;
				break;
			case "W" :
				$jp_pole [] = $j + 1;
				break;
			case "Y" :
				$jp_pole [] = $j + 1;
				break;
			
			case "I" :
				$jp_pole [] = $j + 1;
				break;
			case "L" :
				$jp_pole [] = $j + 1;
				break;
			case "A" :
				$jp_pole [] = $j + 1;
				break;
			case "G" :
				$jp_pole [] = $j + 1;
				break;
			case "V" :
				$jp_pole [] = $j + 1;
				break;
		}
	}
	
	return $jp_pole;
}
function pucky($sekvence, $polepucku) {
	for($i = 0; $i < strlen ( $sekvence ); $i ++) {
		$styl = false;
		
		for($j = 0; $j < count ( $polepucku ); $j ++) {
			if ($i + 1 == $polepucku [$j]) {
				// echo '<BR>'.$i.' pp:'.$polepucku[$j];
				echo '<U>';
				$styl = true;
			}
		}
		
		switch ($sekvence [$i]) {
			case "F" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">F</FONT>';
				else
					echo '<FONT class="jednicky">F</FONT>';
				break;
			case "H" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">H</FONT>';
				else
					echo '<FONT class="jednicky">H</FONT>';
				break;
			case "W" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">W</FONT>';
				else
					echo '<FONT class="jednicky">W</FONT>';
				break;
			case "Y" :
				if ($styl == true)
					echo '<FONT class="jednickypucek">Y</FONT>';
				else
					echo '<FONT class="jednicky">Y</FONT>';
				break;
			
			case "I" :
				if ($styl == true)
					echo '<FONT class="dvojkypucek">I</FONT>';
				else
					echo '<FONT class="dvojky">I</FONT>';
				break;
			case "L" :
				if ($styl == true)
					echo '<FONT class="dvojkypucek">L</FONT>';
				else
					echo '<FONT class="dvojky">L</FONT>';
				break;
			
			case "A" :
				if ($styl == true)
					echo '<FONT class="trojkypucek">A</FONT>';
				else
					echo '<FONT class="trojky">A</FONT>';
				break;
			case "G" :
				if ($styl == true)
					echo '<FONT class="trojkypucek">G</FONT>';
				else
					echo '<FONT class="trojky">G</FONT>';
				break;
			case "V" :
				if ($styl == true)
					echo '<FONT class="trojkypucek">V</FONT>';
				else
					echo '<FONT class="trojky">V</FONT>';
				break;
			default :
				echo $sekvence [$i];
		}
		
		for($j = 0; $j < count ( $polepucku ); $j ++) {
			if ($i + 1 == $polepucku [$j]) {
				echo '</U>';
			}
		}
	}
}
function relativnivzdalenost($sekvence, $pozicepole) {
	$predchozipozice = false;
	
	for($i = 0; $i < strlen ( $sekvence ); $i ++) {
		for($j = 0; $j < count ( $pozicepole ); $j ++) {
			if ($i + 1 == $pozicepole [$j]) {
				if ($predchozipozice != false) {
					echo ($pozicepole [$j] - $predchozipozice - 1) . '&nbsp;&nbsp;&nbsp;';
				}
				
				echo '<SPAN class="jednicky">' . $sekvence [$i] . '</SPAN>&nbsp;&nbsp;&nbsp;';
				$predchozipozice = $pozicepole [$j];
			}
		}
	}
}

// #############################################################################

error_reporting ( E_WARNING );

if (! isset ( $_REQUEST ['protein'] ))
	$_REQUEST ['protein'] = 'HHVKDGYIVDDVNCTIYFILYFCGRNAYWAHCNELECTLKLIKGESGYCQWASPYGNACYCYTKGVPDWIHVLRTKGPGRCW';

?>
<H1>Protein residue clusters</H1>
	Welcome in protein residues page. You can check cluster or clusters of
	your favourite protein :)
	<BR>Type protein sequence to input box and then submit button "Show
	clusters".
	<BR>You'll see clusters of :
	<P>
		class 1 : <SPAN class="jednickypucek">H,F,Y,W</SPAN> <BR>class 2 : <SPAN
			class="dvojkypucek">I,L</SPAN> <BR>class 3 : <SPAN
			class="trojkypucek">A,G,V</SPAN>
	</P>
	
	
	<HR>
	<FORM ACTION="index.php" METHOD="POST">
		<H2>Insert protein sequence : (in UPPERCASE)</H2>
		<P>
			<INPUT TYPE="text" NAME="protein"
				VALUE="<?php echo $_REQUEST['protein']; ?>" SIZE="125"
				MAXLENGTH="500"> <BR>
			<INPUT TYPE="submit" VALUE="Show clusters ...">
	
	</FORM>
<?php

$protein = $_REQUEST ['protein'];
echo '<HR><P><H3>Protein residue clusters :</H3><P>';
pucky ( $protein, vzdvpolipole ( pozicepole ( $protein ) ) );
echo '<HR><P><H3>Relative distance of class 1 residues :</H3><P>';

relativnivzdalenost ( $protein, jednickypozicepole($protein));
?>
<BR>
	<BR>&nbsp;
</BODY>
</HTML>